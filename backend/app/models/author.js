// app/models/bear.js

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var AuthorSchema   = new Schema({
    name: String,
    country: String,
    books: [{type: Schema.Types.ObjectId, ref: 'Book'}]
});

module.exports = mongoose.model('Author', AuthorSchema);
