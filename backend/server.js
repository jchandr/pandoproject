var express    = require('express');
var app        = express();
var bodyParser = require('body-parser');
var mongoose   = require('mongoose');

var Book     = require('./app/models/book');
var Author     = require('./app/models/author');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  )
  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET')
    return res.status(200).json({})
  }
  next()
})

var port = process.env.PORT || 8080;

var bookRoutes = express.Router();
var authorRoutes = express.Router();
mongoose.connect('mongodb://localhost:27017/local', { useNewUrlParser: true });

bookRoutes.get('/', function(req, res) {
  Book.find({}).populate('author').exec((mongoErr, mongoRes) => {
    if(mongoErr) {
      return res.status(500).json({})
    } else {
      return res.status(200).json(mongoRes)
    }
  })
});

bookRoutes.get('/:bookId', function(req, res) {
  Book.findOne({_id: req.params.bookId}).populate({ path: 'author', populate: {path: 'books'}}).exec((mongoErr, mongoRes) => {
    if(mongoErr) {
      return res.status(500).json({})
    } else {
      return res.status(200).json(mongoRes)
    }
  })
});

bookRoutes.delete('/:bookId', function(req, res) {
  Book.deleteOne({_id: req.params.bookId}).exec((mongoErr, mongoRes) => {
    if(mongoErr) {
      return res.status(500).json({})
    } else {
      return res.status(202).json({})
    }
  })
});

bookRoutes.post('/', function(req, res) {
  Author.findOne({
    name: req.body.author.name
  }).exec((err, existingAuthor) => {
    if(err) {
      return res.status(500).json({})
    }
    if(existingAuthor === null) {
      //this is a new author
      var newAuthor = new Author()
      newAuthor.name = req.body.author.name
      newAuthor.country = 'india'
      var newBook = new Book()
      newBook.name = req.body.bookInfo.name
      newBook.author = newAuthor
      newAuthor.books.push(newBook)
      newBook.save()
      newAuthor.save()
      return res.status(201).json({})
    } else {
      // this is an existing author
      var newBook = new Book()
      newBook.name = req.body.bookInfo.name
      newBook.author = existingAuthor
      newBook.save()
      existingAuthor.books.push(newBook._id)
      existingAuthor.save()
      return res.status(201).json({})
    }
  })
});

authorRoutes.get('/', function(req, res) {
  Author.find({}).populate('books').exec((mongoErr, mongoRes) => {
    if(mongoErr) {
      return res.status(500).json({})
    } else {
      return res.status(200).json(mongoRes)
    }
  })
});

authorRoutes.get('/:authorId', function(req, res) {
  Author.findOne({_id: req.params.authorId}).populate('books').exec((mongoErr, mongoRes) => {
    if(mongoErr) {
      return res.status(500).json({})
    } else {
      return res.status(200).json(mongoRes)
    }
  })
})

authorRoutes.delete('/:authorId', function(req, res) {
  Author.findOne({_id: req.params.authorId}).exec((mongoErr, mongoRes) => {
    if(mongoErr) {
      return res.status(500).json({})
    } else {
      Book.deleteMany({
        author: mongoRes
      }).exec((mongoErr1, mongoRes1) => {
        if(mongoErr) {
          return res.status(500).json({})
        } else {
          Author.deleteOne({_id: req.params.authorId}).exec((mongoErr2, mongoRes2) => {
            if(mongoErr) {
              return res.status(500).json({})
            } else {
              return res.status(202).json({})
            }
          })
        }
      })
    }
  })
})

authorRoutes.post('/', function(req, res) {
  Author.findOne({
    name: req.body.author.name
  }).exec((err, existingAuthor) => {
    if(err) {
      return res.status(500).json({})
    }
    if(existingAuthor === null) {
      //this is a new author
      var newAuthor = new Author()
      newAuthor.name = req.body.author.name
      newAuthor.country = 'india'
      newAuthor.save()
      return res.status(201).json({})
    } else {
      // this is an existing author
      return res.status(409).json({})
    }
  })
});

app.use('/book', bookRoutes);
app.use('/author', authorRoutes);

app.listen(port);
console.log('listening on localhost: ' + port);
