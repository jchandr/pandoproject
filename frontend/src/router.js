import Vue from 'vue'
import Router from 'vue-router'
import Book from './views/book/index.vue'
import CreateBook from './views/book/create.vue'
import ShowBook from './views/book/show.vue'
import Author from './views/author/index.vue'
import CreateAuthor from './views/author/create.vue'
import ShowAuthor from './views/author/show.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/book/',
      name: 'Book',
      component: Book
    },
    {
      path: '/book/create',
      name: 'CreateBook',
      component: CreateBook
    },
    {
      path: '/book/:bookId',
      name: 'ShowBook',
      component: ShowBook,
      props: true
    },
    {
      path: '/author',
      name: 'Author',
      component: Author
    },
    {
      path: '/author/create',
      name: 'AuthorCreate',
      component: CreateAuthor
    },
    {
      path: '/author/:authorId',
      name: 'AuthorShow',
      component: ShowAuthor,
      props: true
    },
    {
      path: '*',
      component: Book,
      redirect: { name: 'Book' }
    }
  ]
})
